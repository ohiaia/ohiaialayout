var gulp = require('gulp'),
	less = require('gulp-less'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	prefixer = require('gulp-autoprefixer'),
	path = require('path');

gulp.task('less', function() {
	return gulp.src('./less/*.less')
		.pipe(less({
			paths: [ path.join(__dirname, 'less','includes') ]
		}))
		.pipe(gulp.dest('./public/css'));
});

gulp.task('image', function(){
	gulp.src('img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('img/build'));
});

gulp.task('watch', function(){
	gulp.watch('less/*.less', ['less']);
});

gulp.task('default', ['less', 'image', 'watch']);